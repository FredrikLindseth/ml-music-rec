import sys
import os
# Append path to use modules outside pycharm environment, e.g. terminal
sys.path.append(os.path.abspath(os.path.join(os.getcwd(), os.pardir)))
from spotipy_tools.spotipy_helpers import save_audio_features_for_list_of_playlists
from helper_functions.helpers import load_pickle


# Add playlist URIs for custom datasets,
# then run this file and uncomment likeable_songs and annoying_songs lines in dataset_config

# TODO: E.g: "spotify:user:jooney:playlist:60FUjmTcUrnewNksBNFVWX"
spotify_uris_likeable_songs = [
  "spotify:user:sgiratch:playlist:6MQ5Zw2EtdtP0PnLtaXbm7"
  "spotify:user:sgiratch:playlist:1Bzx2Ay8AFfm0NPBq8XKqr"
  "spotify:user:sgiratch:playlist:5KSmurI65kSDJt8t3J8qAm"
  "spotify:user:sgiratch:playlist:175ah4uQ1Y4Wp1dHbRnRo4"
  "spotify:user:sgiratch:playlist:5hLoosOxz76vPKp2JqahAb"
  "spotify:user:nrkp13:playlist:6rFym3Z5knDzl8QmEDhAdC"
  "spotify:user:kwinsents:playlist:5zD6PzGOPhpyhz21WErdD8"
  "spotify:user:spotify:playlist:37i9dQZF1DX0YKekzl0blG"
  "spotify:user:overagnar:playlist:0GEKEfozxkIHEkgs2jXpvR"
  "spotify:user:spotify:playlist:37i9dQZF1E9RpHfW4XMSzg"
  "spotify:user:spotify:playlist:37i9dQZF1E9RpHfW4XMSzg"
]

# TODO: F. eks: "spotify:user:spotifycharts:playlist:37i9dQZEVXbJvfa0Yxg7E7"
spotify_uris_annoying_songs = [
  "spotify:user:spotify:playlist:37i9dQZF1DX7Hh2OReLFE9"
  "spotify:user:spotifycharts:playlist:37i9dQZEVXbJvfa0Yxg7E7"
  "spotify:user:spotifycharts:playlist:37i9dQZEVXbLiRSasKsNU9"
  "spotify:user:annick1:playlist:3xLjGmLhVO0LqamavdYK3i"
]

save_audio_features_for_list_of_playlists(spotify_uris_likeable_songs, "../playlist_features/likeable_songs")
save_audio_features_for_list_of_playlists(spotify_uris_annoying_songs, "../playlist_features/annoying_songs")

print("")